from email.mime import base
from posixpath import basename
from django.urls import path, include
from .views import *
from rest_framework.routers import DefaultRouter
router = DefaultRouter()
router.register(r'food', FoodViewSet, basename = 'Food')
router.register(r'ingridient', IngridientViewSet, basename = 'Ingridient')
router.register(r'cook', CookViewSet, basename = 'Cook')
urlpatterns=router.urls