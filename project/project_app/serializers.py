from rest_framework import serializers
from .models import *

class FoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = ('id','name', 'price','price','cooking_date', 'cook')

class IngridientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('id','name', 'supplier','supply_date','use_before', 'food')

class CookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cook
        fields = ('id','FIO', 'job_name','salary')
