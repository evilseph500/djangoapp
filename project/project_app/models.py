from email.policy import default
from operator import mod
import re
from statistics import mode
from tabnanny import verbose
from time import time
from turtle import title
from xmlrpc.client import DateTime
from django.db import models
from django.utils import timezone

class Cook(models.Model):
    FIO = models.CharField(verbose_name='ФИО',max_length=100)
    job_name = models.TextField(verbose_name='Должность')
    salary = models.IntegerField(verbose_name='Зарплата')
    def __str__(self):
        return self.FIO

class Food(models.Model):
    cook = models.ForeignKey(Cook, on_delete=models.CASCADE, verbose_name='Повар')
    name = models.CharField(verbose_name='Название блюда', max_length=100)
    price = models.IntegerField(verbose_name='Цена')
    cooking_date = models.DateTimeField(verbose_name='Дата приготовления', default = timezone.now)
    def __str__(self):
        return self.name

class Ingredient(models.Model):
    food = models.ForeignKey(Food, on_delete=models.CASCADE, verbose_name='Еда')
    name = models.CharField(verbose_name='Название ингридиента', max_length=100)
    supplier = models.CharField(verbose_name='Поставщик', max_length=100)
    supply_date = models.DateTimeField(verbose_name='Дата поставки', default=timezone.now)
    use_before = models.DateTimeField(verbose_name='Годен до', default=timezone.now)
    def __str__(self):
        return self.name
# Create your models here.