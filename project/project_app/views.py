from gc import get_objects
from django.shortcuts import render, get_object_or_404
from .models import *
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import *
from rest_framework import viewsets
# Create your views here.

class FoodViewSet(viewsets.ModelViewSet):
    serializer_class = FoodSerializer
    queryset = Food.objects.all()

class IngridientViewSet(viewsets.ModelViewSet):
    serializer_class = IngridientSerializer
    queryset = Ingredient.objects.all()

class CookViewSet(viewsets.ModelViewSet):
    serializer_class = CookSerializer
    queryset = Cook.objects.all()